#include <cstdlib>
#include <cmath>
#include <iostream>
#include "Bmp.hpp"

#define STR_EQ 0
#define TAB "\t"

int main(int argc, char *argv[]) {
	if (argc == 3) {
		if (strcmp(argv[1], "noop") == STR_EQ) {
			std::cerr << "Resave operation called: no transformation required." << std::endl;
			Bmp bmp;
			bmp.read();
			bmp.write();
		}
		else if (strcmp(argv[1], "rotate") == STR_EQ) {
			double angle = strtol(argv[2], nullptr, 10);
			Bmp bmp;
			bmp.read();
			bmp.rotate(angle * M_PI / 180);
			bmp.write();
		}
		else if (strcmp(argv[1], "scale") == STR_EQ) {
			double ratio = strtod(argv[2], nullptr);
			Bmp bmp;
			bmp.read();
			bmp.scale(ratio);
			bmp.write();
		}
	}

	std::cout << "bmp manipulation utility" << std::endl;
	std::cout << "Usage:" << std::endl;
	std::cout << TAB << argv[0]
			<< " rotate <angle_in_degrees>" << std::endl;
	std::cout << TAB << argv[0]
			<< " scale <ratio>" << std::endl;
	return EXIT_SUCCESS;
}
