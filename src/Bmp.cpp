#include <algorithm>
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include "Bmp.hpp"

Bmp::Bmp()
	: palette(nullptr)
{
}

Bmp::~Bmp() {
	if (palette) delete palette;
}

void Bmp::read() {
	std::cin.read(header.bytes, sizeof(header.bytes));
	if (header.data.bit_count < 8) {
		std::cerr << "Images with palette are not supported." << std::endl;
		exit(EXIT_FAILURE);
		// non reachable
		int paletteSize = 4 * (1 << header.data.bit_count);
		palette = new char[paletteSize];
		std::cin.read(palette, paletteSize);
	}
	else if (header.data.bit_count % 8) {
		std::cerr << "Images with non-divisable by 8 are not supported." << std::endl;
		exit(EXIT_FAILURE);
	}
	if (header.data.compression != 0) {
		std::cerr << "Compressed images are not supported." << std::endl;
		exit(EXIT_FAILURE);
	}
	bitmap.assign(header.data.height, std::vector<char>(get_line_bytes()));
	for (auto &line : bitmap) {
		std::cin.read(line.data(), line.size());
	}
}

void Bmp::write() {
	std::cout.write(header.bytes, sizeof(header.bytes));
	if (header.data.bit_count < 8) {
		int paletteSize = 4 * (1 << header.data.bit_count);
		std::cout.write(palette, paletteSize);
	}
	for (auto line : bitmap) {
		std::cout.write(line.data(), line.size());
	}
}

void Bmp::rotate(double angle) {
	int w = header.data.width;
	int h = std::abs(header.data.height);
	int hsign = header.data.height / h;
	int byte_count = header.data.bit_count / 8;
	std::vector<std::vector<char>> result(h, std::vector<char>(get_line_bytes()));
	for (int i = 0; i < std::abs(header.data.height); ++i) {
		for (int j = 0; j < get_line_bytes(); ++j) {
			int m = j % byte_count;
			j /= byte_count;
			std::size_t y = h / 2 + (i - h / 2) * std::cos(angle) + (j - w / 2) * std::sin(angle);
			std::size_t x = w / 2 + (j - w / 2) * std::cos(angle) - (hsign) * (i - h / 2) * std::sin(angle);
			y = std::max(0U, std::min(bitmap.size() - 1, y));
			x = std::max(0U, std::min(bitmap[y].size() / byte_count - 1, x));
			j = j * byte_count + m;
			x = x * byte_count + m;
			/*
			std::size_t y = std::min(bitmap.size() - 1, std::size_t(i / ratio));
			std::size_t x = std::min(
					bitmap[y].size() - 1,
					std::size_t(j / byte_count / ratio) * byte_count + (j % byte_count)
			);
			*/
			result[i][j] = bitmap[y][x];
		}
	}
	bitmap.swap(result);
}

void Bmp::scale(double ratio) {
	header.data.size_image = unsigned(header.data.size_image * ratio * ratio);
	header.data.width = int(header.data.width * ratio);
	header.data.height = int(header.data.height * ratio);
	std::vector<std::vector<char>> result(
			std::abs(header.data.height),
			std::vector<char>(get_line_bytes())
	);
	int byte_count = header.data.bit_count / 8;
	for (int i = 0; i < std::abs(header.data.height); ++i) {
		for (int j = 0; j < get_line_bytes(); ++j) {
			int m = j % byte_count;
			j /= byte_count;
			std::size_t y = std::size_t(i / ratio);
			std::size_t x = std::size_t(j / ratio);
			y = std::min(bitmap.size() - 1, y);
			x = std::min(bitmap[y].size() / byte_count - 1, x);
			j = j * byte_count + m;
			x = x * byte_count + m;
			result[i][j] = bitmap[y][x];
		}
	}
	bitmap.swap(result);
}

int Bmp::get_line_bytes() {
	int lineBytes = header.data.bit_count / 8 * header.data.width;
	if (lineBytes % 4) {
		lineBytes += 4 - (lineBytes % 4);
	}
	return lineBytes;
}
