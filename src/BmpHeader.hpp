#pragma once
#include <cstdint>

#pragma pack(push, 1)
union BmpHeader {
	struct {
		std::uint16_t file_type;
		std::uint32_t file_size;
		std::uint16_t reserved1;
		std::uint16_t reserved2;
		std::uint32_t offset_data;
		std::uint32_t size;
		std::int32_t width;
		std::int32_t height;
		std::uint16_t planes;
		std::uint16_t bit_count;
		std::uint32_t compression;
		std::uint32_t size_image;
		std::int32_t x_pixels_per_meter;
		std::int32_t y_pixels_per_meter;
		std::uint32_t colors_used;
		std::uint32_t colors_important;
	} data;
	char bytes[54];
};
#pragma pack(pop)
