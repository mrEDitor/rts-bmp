#pragma once
#include <vector>
#include "BmpHeader.hpp"

/**
 * Bmp manipulation class
 */
class Bmp {

public:
	Bmp();
	~Bmp();

	/**
	 * Read file from stdin.
	 */
	void read();

	/**
	 * Write file to stdout.
	 */
	void write();

	/**
	 * Rotate image.
	 * @param angle in radians
	 */
	void rotate(double angle);

	/**
	 * Scale image.
	 * @param ratio
	 */
	void scale(double ratio);

private:

	/**
	 * Header.
	 */
	BmpHeader header;

	/**
	 * Pallete.
	 */
	char *palette;

	/**
	 * Bitmap.
	 */
	std::vector<std::vector<char>> bitmap;

	/**
	 * Get count of bytes to store bitmap line.
	 */
	int get_line_bytes();
};
